package com.github.klawru.lib.contract.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MathUtilsTest {
    @Test
    void testTenPow() {
        assertEquals(10L, MathUtils.tenPow(1));
        assertEquals(Long.MAX_VALUE, MathUtils.tenPow(19));
    }
}

