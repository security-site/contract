package com.github.klawru.lib.contract.validation;

import org.hibernate.validator.HibernateValidator;
import org.hibernate.validator.messageinterpolation.ExpressionLanguageFeatureLevel;
import org.hibernate.validator.spi.messageinterpolation.LocaleResolverContext;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Locale;
import java.util.Set;

import static org.hibernate.validator.messageinterpolation.ExpressionLanguageFeatureLevel.BEAN_PROPERTIES;
import static org.junit.jupiter.api.Assertions.assertEquals;

class VerifiableConstrainValidatorTest {
    private static Validator validator;

    @BeforeAll
    static void setUp() {
        ValidatorFactory factory = Validation.byProvider(HibernateValidator.class)
                .configure()
                .constraintExpressionLanguageFeatureLevel(BEAN_PROPERTIES)
                .customViolationExpressionLanguageFeatureLevel(BEAN_PROPERTIES)
                .localeResolver(context -> Locale.forLanguageTag("RU"))
                .buildValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    void isValid() {
        Set<ConstraintViolation<SimpleTestRequest>> validate = validator.validate(new SimpleTestRequest(1, 1));
        assertEquals(1, validate.size());
        assertEquals("Пример Локализации 1=1", validate.iterator().next().getMessage());
    }
}