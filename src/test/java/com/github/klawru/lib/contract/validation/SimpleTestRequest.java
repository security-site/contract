package com.github.klawru.lib.contract.validation;

import lombok.AllArgsConstructor;
import lombok.Value;
import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;

@Verify
@Value
@AllArgsConstructor
public class SimpleTestRequest implements Verifiable {
    int first;
    int second;

    @Override
    public boolean isValid(HibernateConstraintValidatorContext context) throws ValidationConstrainException {
        if (first == second)
            throw new ValidationConstrainException("{exception_message}");
        return true;
    }
}
