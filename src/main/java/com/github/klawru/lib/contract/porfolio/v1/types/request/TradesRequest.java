package com.github.klawru.lib.contract.porfolio.v1.types.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import javax.annotation.Nullable;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Value
@Builder
@AllArgsConstructor
public class TradesRequest {
    @Nullable
    LocalDate from;
    @Valid
    @Nullable
    @NotNull
    List<@NotNull UUID> fileIdList;

    public static TradesRequest of() {
        return new TradesRequest(null, null);
    }

    public static TradesRequest of(LocalDate from, List<UUID> fileIdList) {
        return new TradesRequest(from, fileIdList);
    }

}
