package com.github.klawru.lib.contract.porfolio.v1.types.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import javax.annotation.Nullable;
import java.time.LocalDate;

@Value
public class DayBalanceRequest {
    @Nullable
    LocalDate from;

    @JsonCreator
    public DayBalanceRequest(@JsonProperty @Nullable LocalDate from) {
        this.from = from;
    }
}
