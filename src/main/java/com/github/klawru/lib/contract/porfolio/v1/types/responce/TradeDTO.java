package com.github.klawru.lib.contract.porfolio.v1.types.responce;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.javamoney.moneta.Money;

import javax.money.MonetaryAmount;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class TradeDTO {
    UUID id;
    LocalDateTime date;
    LocalDateTime settlementDate;
    String ticker;
    String exchange;
    String isin;
    Operation operation;
    MonetaryAmount price;
    MonetaryAmount sum;
    Integer quantity;
    Integer totalQuantity;
    MonetaryAmount nkd;
    MonetaryAmount fee;
    UUID fileId;
    String note;
}
