package com.github.klawru.lib.contract.security.v1.types.responce.currency;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

@Value
public class RateHistoryDTO {
    @Nonnull
    LocalDate date;
    @Nonnull
    BigDecimal rate;

    @JsonCreator
    public RateHistoryDTO(@Nonnull LocalDate date, @Nonnull BigDecimal rate) {
        this.date = date;
        this.rate = rate;
    }

    BigDecimal getInverted() {
        return BigDecimal.ONE.divide(rate, RoundingMode.HALF_EVEN);
    }
}
