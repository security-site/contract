package com.github.klawru.lib.contract.security.v1.types.responce;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class SecurityPriceDTO {
    String ticker;
    String exchange;
    LocalDateTime time;
    BigDecimal max;
    BigDecimal min;
    BigDecimal open;
    BigDecimal last;
    String currency;
    BigDecimal value; //Объем сделок за день
    Long numTrades; //Количество сделок за день, штук

    public SecurityPriceDTO(String ticker, String exchange, HistoryDTO historyDTO) {
        this.ticker = ticker;
        this.exchange = exchange;
        this.time = LocalDateTime.of(historyDTO.date, LocalTime.MIN);
        this.max = historyDTO.max;
        this.min = historyDTO.min;
        this.open = historyDTO.open;
        this.last = historyDTO.close;
        this.currency = historyDTO.currency;
        this.value = historyDTO.value;
        this.numTrades = historyDTO.numTrades;
    }

    public static SecurityPriceDTO of(String ticker, String exchange, HistoryDTO historyDTO) {
        return new SecurityPriceDTO(ticker, exchange, historyDTO);
    }
}
