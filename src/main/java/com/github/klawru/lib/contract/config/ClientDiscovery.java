package com.github.klawru.lib.contract.config;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ClientDiscovery {
    public final String PORTFOLIO = "portfolio-manager";
    public final String DBCACHE = "dbcache";
    public final String EXCHANGE_CLIENTS = "exchange-clients";
}
