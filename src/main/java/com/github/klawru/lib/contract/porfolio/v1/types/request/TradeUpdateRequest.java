package com.github.klawru.lib.contract.porfolio.v1.types.request;

import com.github.klawru.lib.contract.porfolio.v1.types.responce.Operation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.util.UUID;

@Value
@Builder
@AllArgsConstructor
@Nullable
public class TradeUpdateRequest {
    @Nonnull
    UUID id;
    Operation operation;
    BigDecimal price;
    BigDecimal sum;
    Integer quantity;
    BigDecimal nkd;
    BigDecimal fee;
    String note;
}
