package com.github.klawru.lib.contract.validation;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidationConstrainValidator implements ConstraintValidator<Verify, Verifiable> {

    @Override
    public void initialize(Verify uniqueItn) {
    }

    @Override
    public boolean isValid(Verifiable verifiable, ConstraintValidatorContext context) {
        HibernateConstraintValidatorContext hibernateContext = context.unwrap(HibernateConstraintValidatorContext.class);
        try {
            return verifiable.isValid(hibernateContext);
        } catch (ValidationConstrainException validationConstrain) {
            hibernateContext.disableDefaultConstraintViolation();
            hibernateContext
                    .buildConstraintViolationWithTemplate(validationConstrain.getMessage())
                    .addConstraintViolation();
            return false;
        }
    }
}