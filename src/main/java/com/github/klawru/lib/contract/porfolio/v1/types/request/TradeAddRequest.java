package com.github.klawru.lib.contract.porfolio.v1.types.request;

import com.github.klawru.lib.contract.porfolio.v1.types.responce.Operation;
import lombok.EqualsAndHashCode;
import lombok.Value;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.validation.constraints.Past;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Value
@EqualsAndHashCode()
public class TradeAddRequest {
    @Nonnull
    String ticker;
    @Nonnull
    String exchange;
    @Past
    @Nonnull
    LocalDateTime date;
    @Nonnull
    Operation operation;
    @Nonnull
    Integer quantity;
    @Nonnull
    BigDecimal price;
    @Nullable
    BigDecimal sum;
    @Nullable
    BigDecimal fee;
    @Nonnull
    String currency;
}
