package com.github.klawru.lib.contract.security.v1.types.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Value;
import lombok.With;

import javax.annotation.Nonnull;
import java.time.LocalDate;

@Value
@JsonIgnoreProperties(ignoreUnknown = true)
public class SecurityRequestDTO {
    @Schema(example = "SBER")
    @Nonnull
    String ticker;
    @Schema(example = "MISX")
    String exchange;
    @With
    LocalDate from;
    @With
    @Nonnull
    SecurityRequestType type;

    @JsonCreator
    public static SecurityRequestDTO of(String ticker, String exchange, LocalDate from, SecurityRequestType type) {
        return new SecurityRequestDTO(ticker, exchange, from, type);
    }

    public SecurityRequestDTO withFrom(LocalDate from, SecurityRequestType type) {
        return this.from != from || this.type != type ? new SecurityRequestDTO(this.ticker, this.exchange, from, type) : this;
    }

}
