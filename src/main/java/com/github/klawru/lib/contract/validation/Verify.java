package com.github.klawru.lib.contract.validation;

import java.lang.annotation.*;
import javax.validation.*;

@Documented
@Constraint(validatedBy = ValidationConstrainValidator.class)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Verify {

    String message() default "Error on validation";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}