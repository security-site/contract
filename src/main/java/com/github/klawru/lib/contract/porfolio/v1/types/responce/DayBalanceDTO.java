package com.github.klawru.lib.contract.porfolio.v1.types.responce;

import lombok.Value;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Value
public class DayBalanceDTO {
    Long id;
    @Nonnull
    LocalDate date;
    /**
     * Средняя цена открытых позиций
     */
    BigDecimal rubSum;
    BigDecimal eurSum;
    BigDecimal usdSum;

}
