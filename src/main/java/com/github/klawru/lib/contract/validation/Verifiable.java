package com.github.klawru.lib.contract.validation;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;

public interface Verifiable {
    boolean isValid(HibernateConstraintValidatorContext context) throws ValidationConstrainException;
}
