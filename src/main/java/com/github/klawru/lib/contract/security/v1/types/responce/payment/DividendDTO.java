package com.github.klawru.lib.contract.security.v1.types.responce.payment;

import com.github.klawru.lib.contract.security.v1.types.responce.MoneyDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.annotation.Nonnull;
import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@ToString(callSuper = true)
public final class DividendDTO extends AbstractPaymentDTO {
    public DividendDTO(@Nonnull LocalDate date, @Nonnull MoneyDTO money) {
        super(date, money);
    }
}
