package com.github.klawru.lib.contract.security.v1;

import com.github.klawru.lib.contract.security.v1.types.request.CurrencyRateRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.request.SecurityRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.ExchangeTypeDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.SecurityDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.SecurityPriceDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.CurrencyRateDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.time.Month;

public interface SecurityInfo {
    LocalDate FROM_MIN_DATE = LocalDate.of(1602, Month.JANUARY, 1);
    String SECURITY_INFO = "security-info";

    String GET_SECURITY = "security";
    String GET_SECURITY_PRICE = "security-price";
    String GET_CURRENCY_RATE = "currency-rate";
    String GET_EXCHANGE_TYPE = "exchange-type";

    Flux<SecurityDTO> getSecurity(SecurityRequestDTO request);

    Mono<SecurityPriceDTO> getSecurityPrice(SecurityRequestDTO request);

    Mono<CurrencyRateDTO> getCurrencyRate(CurrencyRateRequestDTO request);

    /**
     * @return возвращает поддерживаемые биржи
     */
    Flux<ExchangeTypeDTO> getExchangeType();
}
