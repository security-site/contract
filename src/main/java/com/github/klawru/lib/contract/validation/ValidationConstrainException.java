package com.github.klawru.lib.contract.validation;

import lombok.Getter;
import lombok.Singular;

import java.util.Collections;
import java.util.List;

public class ValidationConstrainException extends Exception {
    @Singular
    @Getter
    List<String> property = Collections.emptyList();

    public ValidationConstrainException(String message) {
        super(message);
    }

    public ValidationConstrainException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidationConstrainException(String message, List<String> property) {
        super(message);
        this.property = property;
    }

    public ValidationConstrainException(String message, List<String> property, Throwable cause) {
        super(message, cause);
        this.property = property;
    }
}
