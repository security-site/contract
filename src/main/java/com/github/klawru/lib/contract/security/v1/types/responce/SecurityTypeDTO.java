package com.github.klawru.lib.contract.security.v1.types.responce;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@Value
public class SecurityTypeDTO {
    @Nonnull
    SecurityType type;
    @Nullable
    SecuritySubType subType;
    @Nonnull
    String raw;

    @JsonCreator
    public SecurityTypeDTO(@Nonnull SecurityType type, @Nullable SecuritySubType subType, @Nonnull String raw) {
        this.type = type;
        this.subType = subType;
        this.raw = raw;
    }
}
