package com.github.klawru.lib.contract.security.v1.types.request;

public enum SecurityRequestType {
    ALL,
    PRICE_ONLY,
}
