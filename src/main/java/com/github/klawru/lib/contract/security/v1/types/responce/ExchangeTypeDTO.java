package com.github.klawru.lib.contract.security.v1.types.responce;

import lombok.*;
import lombok.extern.jackson.Jacksonized;

import javax.annotation.Nonnull;
import java.math.BigDecimal;

@Value
@Builder
@AllArgsConstructor
public class ExchangeTypeDTO {
    @Nonnull
    String id;
    @Nonnull
    String name;
}
