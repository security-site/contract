package com.github.klawru.lib.contract.security.v1.types.responce;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;

import javax.annotation.Nonnull;

@Value
public class SecurityId {
    @Nonnull
    String ticker;
    @Nonnull
    String exchange;

    @JsonCreator
    public SecurityId(@Nonnull String ticker, @Nonnull String exchange) {
        this.ticker = ticker;
        this.exchange = exchange;
    }
}
