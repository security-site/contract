package com.github.klawru.lib.contract.security.v1.types.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.CurrencyPairDTO;
import lombok.Value;
import lombok.With;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDate;

@Value
public class CurrencyRateRequestDTO {

    @Nonnull
    CurrencyPairDTO currency;
    /**
     * Дата с которой возвращать значения. Если не указано возращаеться только последняя стоймость
     */
    @Nullable
    @With
    LocalDate from;

    @JsonCreator
    public static CurrencyRateRequestDTO of(@Nonnull CurrencyPairDTO currency, @Nullable LocalDate from) {
        return new CurrencyRateRequestDTO(currency, from);
    }

    public static CurrencyRateRequestDTO of(@Nonnull String from, @Nonnull String to, @Nullable LocalDate fromDate) {
        return new CurrencyRateRequestDTO(CurrencyPairDTO.of(from, to), fromDate);
    }
}
