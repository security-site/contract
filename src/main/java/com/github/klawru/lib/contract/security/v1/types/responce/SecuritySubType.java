package com.github.klawru.lib.contract.security.v1.types.responce;

public enum SecuritySubType {

    UNRECOGNIZED,
    COMMON_SHARE, //Акция обыкновенная
    PREFERRED_SHARE, //Акция привилегированная
    DEPOSITARY_RECEIPT, //Депозитарная расписка
    OFZ_BOND, //ОФЗ
    CB_BOND, //Облигация центрального банка
    SUB_FEDERAL_BOND, //Региональная облигация
    MUNICIPAL_BOND, //Муниципальная облигация
    CORPORATE_BOND, //Корпоративная облигация
    EXCHANGE_BOND, //Биржевая облигация
    IFI_BOND, //Государственная облигация
    EURO_BOND, //Еврооблигации
    PUBLIC_PPIF, //Пай открытого ПИФа
    INTERVAL_PPIF, //Пай закрытого ПИФа
    RTS_INDEX, //Ипотечный сертификат
    PRIVATE_PPIF, //ETF
    STOCK_MORTGAGE, //Индекс фондового рынка
    ETF_PPIF, //Пай биржевого ПИФа
    STOCK_INDEX, //Валюта
    EXCHANGE_PPIF, //Металл золото
    STOCK_DEPOSIT, //Металл серебро
    NON_EXCHANGE_BOND, //Валютный фьючерс
    STATE_BOND, //Валютный фиксинг
    CURRENCY_INDEX, //Фьючерс
    CURRENCY_SUB, //Опцион
    METAL,
    OTHER_SUB,
}
