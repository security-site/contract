package com.github.klawru.lib.contract.security.v1.types.responce;

import com.github.klawru.lib.contract.security.v1.types.responce.payment.AbstractPaymentDTO;
import lombok.*;
import org.javamoney.moneta.Money;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.money.MonetaryAmount;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString(exclude = {"history", "payments"})
public class SecurityDTO {
    @Nonnull
    String ticker;
    @Nonnull
    String exchange;
    String isin;
    @Nonnull
    String shortName;
    String fullName;
    String shortLatName;
    String fullLatName;
    /**
     * Last face value
     */
    MoneyDTO faceValue;

    Integer lotSize;
    LocalDate issueDate;
    Long issueSize;

    MonetaryAmount issueCapitalization;
    @Nonnull
    SecurityTypeDTO type;
    @Nullable
    SectorDTO sector;
    @Nullable
    Boolean qualifiedInvestors;
    @Nullable
    String site;
    List<HistoryDTO> history;
    List<AbstractPaymentDTO> payments;
    @Nullable
    Instant lastUpdate;

    public SecurityId getId() {
        return new SecurityId(ticker, exchange);
    }
}
