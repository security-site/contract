package com.github.klawru.lib.contract.security.v1.types.responce;

import lombok.AllArgsConstructor;
import lombok.Value;

import javax.annotation.Nonnull;

@Value
@AllArgsConstructor
public class SectorDTO {
    SectorType type;
    @Nonnull
    String raw;
}
