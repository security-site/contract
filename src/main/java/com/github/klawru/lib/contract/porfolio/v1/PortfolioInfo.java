package com.github.klawru.lib.contract.porfolio.v1;

import com.github.klawru.lib.contract.porfolio.v1.types.request.*;
import com.github.klawru.lib.contract.porfolio.v1.types.responce.DayBalanceDTO;
import com.github.klawru.lib.contract.porfolio.v1.types.responce.StockPortfolioDTO;
import com.github.klawru.lib.contract.porfolio.v1.types.responce.TradeDTO;
import org.springframework.core.io.buffer.DataBuffer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.validation.Valid;

public interface PortfolioInfo {
    String PORTFOLIO_INFO = "portfolio-info";

    String GET_PORTFOLIO = "portfolio";
    String GET_DAY_BALANCE = "day-balance";
    String GET_TRADES = "trades";
    String ADD_TRADES = "trades-add";
    String UPDATE_TRADES = "trades-update";
    String DELETE_TRADES = "trades-delete";
    String UPLOAD_FILE = "upload-file";

    String MIME_FILE_NAME = "message/x.upload.file.name";
    String MIME_FILE_SIZE = "message/x.upload.file.size";
    String MIME_BANK_IMPORT = "message/x.upload.bank.import";

    Flux<StockPortfolioDTO> getPortfolio();

    Flux<DayBalanceDTO> getDayBalance(DayBalanceRequest request);

    Flux<TradeDTO> getTrades(TradesRequest request);

    Mono<Long> addTrade(TradeAddRequest request);

    Mono<Void> updateTrades(TradeUpdateRequest request);

    Mono<Void> deleteTrades(TradeDeleteRequest request);

    Mono<Long> uploadFile(@Nonnull Flux<DataBuffer> filePartMono,
                          @Nonnull String bankImportType,
                          @Nonnull String fileName,
                          @Nullable Long contentLength);
}
