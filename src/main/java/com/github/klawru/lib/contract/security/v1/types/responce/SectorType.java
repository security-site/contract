package com.github.klawru.lib.contract.security.v1.types.responce;

public enum SectorType {
    UNRECOGNIZED,
    TRANSPORT,
    LOGISTICS,
    CHEMISTRY,
    HOLDINGS,
    RETAIL_TRADE,
    ENERGY,
    ENERGY_RESOURCES,
    FINANCE_BANKING,
    INDUSTRY,
    METALS_MINING,
    REAL_ESTATE,
    TELECOMMUNICATIONS,
    TECHNOLOGY,
    AGRICULTURE,
    HEALTHCARE,
    FUNDS,
    CONSTRUCTION,
}
