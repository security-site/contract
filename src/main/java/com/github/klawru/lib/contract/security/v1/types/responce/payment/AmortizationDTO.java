package com.github.klawru.lib.contract.security.v1.types.responce.payment;

import com.github.klawru.lib.contract.security.v1.types.responce.MoneyDTO;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.time.LocalDate;


@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public final class AmortizationDTO extends CouponDTO {


    public AmortizationDTO(@Nonnull LocalDate date, @Nonnull MoneyDTO money, MoneyDTO faceValue, BigDecimal valuePercent, MoneyDTO valueRub) {
        super(date, money, faceValue, valuePercent, valueRub);
    }
}
