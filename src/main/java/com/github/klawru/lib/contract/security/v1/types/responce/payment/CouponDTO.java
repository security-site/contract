package com.github.klawru.lib.contract.security.v1.types.responce.payment;

import com.github.klawru.lib.contract.security.v1.types.responce.MoneyDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public sealed class CouponDTO extends AbstractPaymentDTO permits AmortizationDTO {
    MoneyDTO faceValue;
    BigDecimal valuePercent;
    MoneyDTO valueRub;

    public CouponDTO(@Nonnull LocalDate date, @Nonnull MoneyDTO money, MoneyDTO faceValue, BigDecimal valuePercent, MoneyDTO valueRub) {
        super(date, money);
        this.faceValue = faceValue;
        this.valuePercent = valuePercent;
        this.valueRub = valueRub;
    }
}
