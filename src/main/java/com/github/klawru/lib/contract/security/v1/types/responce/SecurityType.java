package com.github.klawru.lib.contract.security.v1.types.responce;

public enum SecurityType {

    UNRECOGNIZED,
    INDEX, //Индексы
    STOCK_SHARES, //Акции
    STOCK_ETF, //Биржевые фонды
    STOCK_BONDS, //Облигации
    CURRENCY, //Валюта
    FUTURES_FORTS, //Фьючерсы
    FUTURES_OPTIONS, //Опционы
    CURRENCY_METAL, //Драгоценные металлы
    OTHER,
}
