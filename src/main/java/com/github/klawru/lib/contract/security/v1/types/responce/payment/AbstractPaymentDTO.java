package com.github.klawru.lib.contract.security.v1.types.responce.payment;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.github.klawru.lib.contract.security.v1.types.responce.MoneyDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.Nonnull;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = CouponDTO.class, name = "coupon"),
        @JsonSubTypes.Type(value = AmortizationDTO.class, name = "amortization"),
        @JsonSubTypes.Type(value = DividendDTO.class, name = "dividend"),
})
public abstract sealed class AbstractPaymentDTO permits CouponDTO, DividendDTO {
    @Nonnull
    LocalDate date;
    @Nonnull
    MoneyDTO money;
}
