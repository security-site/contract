package com.github.klawru.lib.contract.security.v1.types.responce;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.javamoney.moneta.Money;

import javax.annotation.Nonnull;
import javax.money.MonetaryAmount;
import java.math.BigDecimal;

/**
 * Класс позволяет хранить суммы меньше, минимального размера для валюты (прим. 0.001 RUB).
 * Из за этого прямое преобразование в JSR Money не всегда возможно.
 */
@Value
@AllArgsConstructor
@EqualsAndHashCode
public class MoneyDTO {
    @Nonnull
    BigDecimal amount;
    @Nonnull
    String currency;

    @JsonCreator
    public static MoneyDTO of(BigDecimal amount, String currency) {
        return new MoneyDTO(amount, currency);
    }

    public static MoneyDTO of(long number, int scale, String currency) {
        return new MoneyDTO(BigDecimal.valueOf(number, scale), currency);
    }

    public MonetaryAmount toAmount() {
        return Money.of(amount, currency);
    }

    public MonetaryAmount toAmount(int quantity) {
        return Money.of(amount.multiply(BigDecimal.valueOf(quantity)), currency);
    }

}
