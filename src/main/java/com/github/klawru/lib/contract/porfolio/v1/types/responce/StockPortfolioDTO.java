package com.github.klawru.lib.contract.porfolio.v1.types.responce;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.money.CurrencyUnit;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Currency;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockPortfolioDTO {
    String ticker;
    String exchange;

    Integer buyQuantity;
    Integer saleQuantity;
    /**
     * Средняя цена открытых позиций
     */
    BigDecimal averagePrice;
    BigDecimal buySumPrice;
    BigDecimal saleSumPrice;
    String priceCurrency;

    BigDecimal buyFee;
    BigDecimal saleFee;
    String feeCurrency;

    BigDecimal dividend;
    String dividendCurrency;

    LocalDateTime firstTransaction;
    LocalDateTime lastTransaction;
}
