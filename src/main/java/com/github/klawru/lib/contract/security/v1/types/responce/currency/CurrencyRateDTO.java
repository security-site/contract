package com.github.klawru.lib.contract.security.v1.types.responce.currency;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.ToString;
import lombok.Value;
import lombok.With;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.util.List;

@Value
@ToString(exclude = "history")
public class CurrencyRateDTO {
    @Nonnull
    CurrencyPairDTO currency;
    @Nonnull
    BigDecimal last;

    @With
    List<RateHistoryDTO> history;

    @JsonCreator
    public static CurrencyRateDTO of(@Nonnull CurrencyPairDTO currency, @Nonnull BigDecimal last, @Nullable List<RateHistoryDTO> historyCurrencyDTO) {
        return new CurrencyRateDTO(currency, last, historyCurrencyDTO);
    }
}
