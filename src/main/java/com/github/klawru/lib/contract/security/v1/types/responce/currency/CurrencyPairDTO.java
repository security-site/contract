package com.github.klawru.lib.contract.security.v1.types.responce.currency;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;

import javax.annotation.Nonnull;

@Value
public class CurrencyPairDTO {
    @Nonnull
    String from;

    @Nonnull
    String to;

    @JsonCreator
    public static CurrencyPairDTO of(@Nonnull String from, @Nonnull String to) {
        return new CurrencyPairDTO(from, to);
    }
}
