package com.github.klawru.lib.contract.security.v1.types.responce;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class HistoryDTO {
    @Nonnull
    LocalDate date;
    BigDecimal price;//Средневзвешенная цена
    BigDecimal max;
    BigDecimal min;
    BigDecimal open;
    BigDecimal close;
    String currency;
    MoneyDTO faceValue;
    BigDecimal value; //Объем сделок за день
    Long numTrades; //Количество сделок за день, штук
}
