package com.github.klawru.lib.contract.porfolio.v1.types.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.UUID;

@Value
@Builder
@AllArgsConstructor
@EqualsAndHashCode()
@Nullable
public class TradeDeleteRequest {
    @Nonnull
    UUID id;
}
