# Contract

Проект Contract содержит контракты для микросервисных взаимодействий.

## Использование

1. Добавьте репозиторий gitlab в ваш проект:

```xml

<repositories>
    <repository>
        <id>central</id>
        <name>Central Repository</name>
        <url>https://repo.maven.apache.org/maven2</url>
        <snapshots>
            <enabled>false</enabled>
        </snapshots>
    </repository>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/groups/13153348/-/packages/maven</url>
    </repository>
</repositories>
```

2. Добавьте токен для репозитория в корень проекта `settings.xml`

```xml

<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
    <servers>
        <server>
            <id>gitlab-maven</id>
            <configuration>
                <httpHeaders>
                    <property>
                        <name>Deploy-Token</name>
                        <value>tokenPassword</value>
                    </property>
                </httpHeaders>
            </configuration>
        </server>
    </servers>
</settings>
```

3. Добавьте зависимость в проект

```xml

<dependency>
    <groupId>com.github.klawru</groupId>
    <artifactId>contract</artifactId>
    <version>1.0</version>
</dependency>
```

## Для локальной разработки

1. Поменяйти версию в pom.xml на другую (пример:`1.1-SNAPSHOT`)
2. Внесите изменения
3. Запустите `maven install`
4. Укажите в вашем проекте измененную версию
